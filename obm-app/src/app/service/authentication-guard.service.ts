import { Injectable } from '@angular/core';
import {CanActivate} from "@angular/router";

@Injectable()
export class AuthenticationGuardService implements CanActivate {

  constructor() { }

  canActivate() {
    console.log('Estas ingresando a la lista de cuartos');
    return confirm('Deseas continuar?');
  }

}
