import { Injectable } from '@angular/core';
import {Room} from "../room/room-list/room";

@Injectable()
export class RoomService {

  private rooms: Room[] = [];

  constructor() { }

  getRooms(): Room[] {
    let room1 = new Room();
    room1.name = 'Standard double room';
    room1.description = 'Dummy room1';
    room1.price = 1000;
    room1.type = 'Standard double room type';
    room1.imagePath = 'http://www.marinabaysands.com/content/dam/singapore/marinabaysands/master/main/home/hotel/deluxe1500x930.jpg';

    this.rooms.push(room1);

    room1 = new Room();
    room1.name = 'Deluxe double room2';
    room1.description = 'Dummy room2';
    room1.price = 2000;
    room1.type = 'Deluxe double room type';
    room1.imagePath = 'http://www.marinabaysands.com/content/dam/singapore/marinabaysands/master/main/home/hotel/deluxe1500x930.jpg';

    this.rooms.push(room1);

    return this.rooms;
  }

}
