import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { RoomComponent } from './room/room.component';
import { HeaderComponent } from './header/header.component';
import { RoomListComponent } from './room/room-list/room-list.component';
import { RoomDetailComponent } from './room/room-detail/room-detail.component';
import { RoomItemComponent } from './room/room-list/room-item.component';
import {RoomService} from "./service/room.service";
import {routing} from "./app.routing";
import { SearchComponent } from './search/search.component';
import {AuthenticationGuardService} from "./service/authentication-guard.service";
import { CutTextPipe } from './common/pipe/cut-text.pipe';

@NgModule({
  declarations: [
    AppComponent,
    RoomComponent,
    HeaderComponent,
    RoomListComponent,
    RoomDetailComponent,
    RoomItemComponent,
    SearchComponent,
    CutTextPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
  ],
  providers: [RoomService,
    AuthenticationGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
