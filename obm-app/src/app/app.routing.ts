import {Routes, RouterModule} from "@angular/router";
import {RoomComponent} from "./room/room.component";
import {AuthenticationGuardService} from "./service/authentication-guard.service";

const APP_ROUTES: Routes = [
    { path: '', redirectTo: '/room', pathMatch: 'full' },
    { path: 'room', component: RoomComponent, canActivate: [AuthenticationGuardService] },
    { path: '**', redirectTo: ''}
];

export const routing = RouterModule.forRoot(APP_ROUTES);