import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cutText'
})
export class CutTextPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    console.log(value);
    return value.toString().substring(2,3).toString();
  }

}
