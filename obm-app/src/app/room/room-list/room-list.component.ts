import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import { Room } from './room';
import {RoomService} from "../../service/room.service";

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {

  rooms: Room[] = [];
  @Output() roomSelected = new EventEmitter<Room>();

  constructor(private roomService: RoomService) { }

  ngOnInit() {
    this.rooms = this.roomService.getRooms();
  }

  onSelected(room: Room) {
    this.roomSelected.emit(room);
  }

}
