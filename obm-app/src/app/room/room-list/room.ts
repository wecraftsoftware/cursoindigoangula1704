export class Room {

  public name: string;
  public type: string;
  public price: number;
  public description: string;
  public imagePath: string;

  constructor() { }
}
