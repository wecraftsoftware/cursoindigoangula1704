import { ObmAppPage } from './app.po';

describe('obm-app App', function() {
  let page: ObmAppPage;

  beforeEach(() => {
    page = new ObmAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
