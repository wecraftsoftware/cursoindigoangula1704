#SNIPPET 1#
	<div class="row">
	  <div class="col-md-12">
	    Search Room
	  </div>
	  <div class="col-md-5">
	    Room list
	  </div>
	  <div class="col-md-7">
	    Rooms detail
	  </div>
	</div>


#SNIPPET 2#
	<a href="#" class="list-group-item clearfix">
	  <div class="pull-left">
	    <h4 class="list-group-item-heading">{{room.name}}</h4>
	    <p class="list-group-item-text">{{room.type}}</p>
	    <p class="list-group-item-text">{{room.price}}</p>
	    <p class="list-group-item-text">{{room.description}}</p>
	  </div>
	  <span class="pull-right">
	      <img class="img-responsive"
	           src="{{room.imagePath}}"
	           style="max-height: 100px;"/>
	  </span>
	</a>


#SNIPPET 3#
	<div class="row">
	  <div class="col-xs-12">
	    <ul class="list-group">
	      
	    </ul>
	  </div>
	</div>


#SNIPPET 4#
	<div class="row">
	  <div class="col-xs-12">
	    <img src="{{selectedRoom?.imagePath}}" alt="" class="img-responsive">
	  </div>
	</div>
	<div class="row">
	  <div class="col-xs-12">
	    <h1>{{selectedRoom?.name}}</h1>
	  </div>
	  <div class="col-xs-12">
	    <button class="btn btn-success">Reservations</button>
	    <button class="btn btn-primary">Book it!</button>
	    <button class="btn btn-danger">Cancel</button>
	  </div>
	</div>
	<hr>
	<div class="row">
	  <div class="col-xs-12">
	    <p>{{selectedRoom?.description}}</p>
	  </div>
	</div>
	<div class="row">
	  <div class="col-xs-12">
	    Datos
	  </div>
	</div>


#SNIPPET 5#
	<div class="row">
	  <form class="form-horizontal" name="roomSearchForm" #roomSearchForm="ngForm" novalidate>
	    <div class="col-md-12">
	      <div class="col-md-2" align="right">
	        <label for="checkin">Check-in:</label>
	      </div>
	      <div class="form-group col-md-2" [ngClass]="{ 'has-error': roomSearchForm.submitted && !checkin.valid }" >
	        <input type="date" class="form-control" name="checkin" id="checkin" placeholder="Check-in"
	               [(ngModel)]="dateCheckIn" #checkin="ngModel" required/>
	      </div>

	      <div class="col-md-2" align="right">
	        <label for="checkout">Check-out:</label>
	      </div>
	      <div class="form-group col-md-2" [ngClass]="{ 'has-error': roomSearchForm.submitted && !checkout.valid }" >
	        <input type="date" class="form-control" name="checkout" id="checkout" placeholder="Check-out"
	               [(ngModel)]="dateCheckOut" #checkout="ngModel" required/>
	      </div>

	      <div class="col-md-2" align="right">
	        <label for="roomtype">Room type:</label>
	      </div>
	      <div class="form-group col-md-2" [ngClass]="{ 'has-error': roomSearchForm.submitted && !roomtype.valid }" >
	        <select class="form-control" name="roomtype" id="roomtype"
	                 [(ngModel)]="roomToSearch.type" #roomtype="ngModel">
	          <option *ngFor="let roomType of roomTypes" >{{ roomType.type }}</option>
	        </select>
	      </div>
	    </div>

	    <div class="col-md-12">
	      <div class="col-md-2" align="right">
	        <label for="rooms">Rooms:</label>
	      </div>
	      <div class="form-group col-md-2" [ngClass]="{ 'has-error': roomSearchForm.submitted && !rooms.valid }" >
	        <input type="number" class="form-control" name="rooms" id="rooms" placeholder="Rooms"
	               [(ngModel)]="numberOfRooms" #rooms="ngModel" pattern="^(0|[1-9][0-9]*)$" required/>
	      </div>

	      <div class="col-md-2" align="right">
	        <label for="adultsperroom">Adults per room:</label>
	      </div>
	      <div class="form-group col-md-2" [ngClass]="{ 'has-error': roomSearchForm.submitted && !adultsperroom.valid }" >
	        <input type="number" class="form-control" name="adultsperroom" id="adultsperroom" placeholder="Adults per room"
	               [(ngModel)]="roomToSearch.adults" #adultsperroom="ngModel" pattern="^(0|[1-9][0-9]*)$" required/>
	      </div>

	      <div class="col-md-2" align="right">
	        <label for="childrenperroom">Children per room:</label>
	      </div>
	      <div class="form-group col-md-2" [ngClass]="{ 'has-error': roomSearchForm.submitted && !childrenperroom.valid }" >
	        <input type="number" class="form-control" name="childrenperroom" id="childrenperroom" placeholder="Children per room"
	               [(ngModel)]="roomToSearch.children" #childrenperroom="ngModel" pattern="^(0|[1-9][0-9]*)$"/>
	      </div>
	    </div>
	    <div class="form-group" align="right">
	      <button [disabled]="searching" class="btn btn-primary">Search</button>
	      <img *ngIf="searching" src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
	    </div>
	  </form>
	</div>

#SNIPPET 6#
     <h3>Please sign up to use all features</h3>
        <form [formGroup]="myForm" (ngSubmit)="onSignup()">
            <div class="form-group">
                <label for="email">E-Mail</label>
                <input formControlName="email" type="email" id="email" #email class="form-control">
                <span *ngIf="!email.pristine && email.errors != null && email.errors['noEmail']">Invalid mail address</span>
        
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input formControlName="password" type="password" id="password" class="form-control">
            </div>
            <div class="form-group">
                <label for="confirm-password">Confirm Password</label>
                <input formControlName="confirmPassword" type="password" id="confirm-password" #confirmPassword class="form-control">
                <span *ngIf="!confirmPassword.pristine && confirmPassword.errors != null && confirmPassword.errors['passwordsNotMatch']">Passwords do not match</span>
            </div>
            <button type="submit" [disabled]="!myForm.valid" class="btn btn-primary">Sign Up</button>
        </form>